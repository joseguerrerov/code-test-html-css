import React from 'react';
import styled from 'styled-components';
import { colors } from '../../theme';

interface IProps extends
  React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
  label: string;
  backgroundColor?: string;
  borderColor?: string;
  labelColor?: string;
}

const Button: React.FC<IProps> = (props) => (
  <StyledButton {...props}>
    {props.label}
  </StyledButton>
);

const StyledButton = styled.button<any>`
  cursor: pointer;
  padding: 0.5rem 0;
  font-size: 0.875rem;
  width: 100%;
  font-weight: 700;
  text-transform: uppercase;
  background-color: ${(props: IProps) => props.backgroundColor};
  border: 2px solid ${(props: IProps) => props.borderColor};
  color: ${(props: IProps) => props.labelColor};
`;

Button.defaultProps = {
  backgroundColor: colors.white.main,
  borderColor: colors.blue.light,
  labelColor: colors.blue.main,
};

export default React.memo<IProps>(Button);
