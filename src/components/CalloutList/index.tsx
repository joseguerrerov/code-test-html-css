import React from 'react';
import styled from 'styled-components';
import CalloutListItem from '../CalloutListItem';
import { breakpoints, colors } from '../../theme';
import Image1 from '../../assets/images/callout-generic-example1.jpg';
import Image2 from '../../assets/images/callout-generic-example2.jpg';
import Underline from '../Underline';

const CalloutList: React.FC = () => (
  <ListContainer className="container">
    <CalloutListItem
      title="CITI SPOTLIGHTS SUMMER  LEADERSHIP PROGRAM"
      imageSrc={Image1}
      imageAlt="Summer leadership program members"
      buttonLabel="learn more"
    >
      The Citi Spotlights Leadership Program empowers youth to become
      leaders in school, at home and in their communities
    </CalloutListItem>
    <CalloutListItem
      title="SUPPORT OUR NONPROFIT  BY DONATING TODAY"
      buttonLabel="learn more"
    >
      Learn how to get amazing access and benefits when you sponsor the theatres and programs.
      Sponsorships range from Corporate to institutional
      <a href="#Corporate LeaderShip" title="Go to Corporate LeaderShip">
        <Underline>Corporate LeaderShip</Underline>
      </a>
      <a href="#Individual Giving" title="Go to Individual Giving">
        <Underline>Individual Giving</Underline>
      </a>
      <a href="#Partnerships" title="Go to Partnerships">
        <Underline>Partnerships</Underline>
      </a>
      <a href="#Institutional Supporters" title="Go to Institutional Supporters">
        <Underline>Institutional Supporters</Underline>
      </a>
    </CalloutListItem>
    <CalloutListItem
      title="DISCOVER OUR THEATRES, BE A PART OF THE MAGIC"
      imageSrc={Image2}
      imageAlt="Empty Theatre"
      buttonLabel="become a sponsor"
      backgroundColor={colors.yellow.main}
      borderColor={colors.yellow.light}
      buttonLabelColor={colors.black.main}
      textColor={colors.black.main}
    >
      <a href="#Corporate LeaderShi" title="Go to Wang Theatre">
        <Underline>Wang Theatre</Underline>
      </a>
      <a href="#Individual Giving" title="Go to Emerson Colonial Theatre">
        <Underline>Emerson Colonial Theatre</Underline>
      </a>
      <a href="#Partnerships" title="Go to Shubert Theatre">
        <Underline>Shubert Theatre</Underline>
      </a>
      <a href="#Institutional Supporters" title="Go to The Centre at Suffolk Down">
        <Underline>The Centre at Suffolk Downs</Underline>
      </a>
    </CalloutListItem>
    <div style={{ clear: 'both' }} />
  </ListContainer>
);

const ListContainer = styled.div`
  @media screen and (min-width: ${breakpoints.tablet}) {
    padding: 0 0.625rem;
    div:nth-child(2){
      margin: 0 24px;
    }
  }
  @media screen and (min-width: ${breakpoints.desktop}) {
    padding: 0;
  }
`;

export default CalloutList;
