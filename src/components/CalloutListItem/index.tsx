import React from 'react';
import styled from 'styled-components';
import Button from '../Button';
import { breakpoints, colors } from '../../theme';

interface IProps {
  title: string;
  buttonLabel: string;
  imageAlt?: string;
  children?: React.ReactNode;
  imageSrc?: string;
  backgroundColor?: string;
  borderColor?: string;
  buttonLabelColor?: string;
  textColor?: string;
}

const CalloutListItem: React.FC<IProps> = (props) => (
  <Container {...props} >
    <Title>{props.title}</Title>
    {props.imageSrc && <StyledImg src={props.imageSrc} alt={props.imageAlt} />}
    <Body textColor={String(props.textColor)}>{props.children}</Body>
    <StyledButton
      label={props.buttonLabel}
      borderColor={props.borderColor}
      labelColor={props.buttonLabelColor}
    />
  </Container>
);

const Container = styled.div`
  background-color: ${(props: IProps) => props.backgroundColor};
  border: 0.625rem ${(props: IProps) => props.borderColor} solid;
  color: ${(props: IProps) => props.textColor};
  padding: 0.9375rem;
  position: relative;
  @media screen and (min-width: ${breakpoints.tablet}) {
    width: calc(33.33% - 16px);
    height: 450px;
    float: left;
    padding: 0.625rem;
  }
  @media screen and (min-width: 890px) {
  height: 472px;
  }
  @media screen and (min-width: ${breakpoints.desktop}) {
    padding: 0.9375rem;
  }
`;

const Title = styled.h3`
  margin: 0 0 0.9375rem 0;
  font-weight: 900;
  font-size: 1.125rem;
  @media screen and (min-width: ${breakpoints.tablet}) {
    font-size: 1rem;
    max-width: 200px;
  }
  @media screen and (min-width: ${breakpoints.desktop}) {
    font-size: 1.125rem;
    max-width: 100%;
  }
`;

const StyledImg = styled.img`
  width: 100%;
`;

const Body = styled.p<{ textColor: string }>`
  font-weight: 400;
  font-size: 1rem;
  line-height: 1.5rem;
  margin: 0.875rem 0;
  a {
    color: ${(props) => props.textColor};
    display: block;
    margin: 10px 0;
  }
  @media screen and (min-width: ${breakpoints.tablet}) {
    font-size: 0.875rem;
  }
  @media screen and (min-width: ${breakpoints.desktop}) {
    font-size: 1rem;
  }
`;

const StyledButton = styled(Button)`
  @media screen and (min-width: ${breakpoints.tablet}) {
    position: absolute;
    bottom: 0.625rem;
    left: 0;
    right: 0;
    margin: auto;
    width: calc(100% - 1.25rem);
  }
  @media screen and (min-width: ${breakpoints.desktop}) {
    padding-left: 1.75rem;
    padding-right: 1.75rem;
    font-size: 1rem;
    bottom: 0.9375rem;
    width: auto;
    left: unset;
    right: 0.9375rem;
  }
`;

CalloutListItem.defaultProps = {
  backgroundColor: colors.blue.main,
  borderColor: colors.blue.light,
  textColor: colors.white.main,
  buttonLabelColor: colors.blue.main,
  imageAlt: 'image',
};

export default React.memo<IProps>(CalloutListItem);
