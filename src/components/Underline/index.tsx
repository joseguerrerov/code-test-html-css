import React from 'react';
import styled from 'styled-components';

interface IProps {
  children: React.ReactNode;
}

const Underline: React.FC<IProps> = ({ children }) => (
  <StyledSpan>{children}</StyledSpan>
);

const StyledSpan = styled.span`
  border-bottom: 1px solid;
`;

export default React.memo<IProps>(Underline);
