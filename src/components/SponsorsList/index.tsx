import React from 'react';
import styled from 'styled-components';
import VerndaleLogo from '../../assets/images/verndale.png';
import PolarLogo from '../../assets/images/polar.png';
import MsgLogo from '../../assets/images/msg.png';
import CitiLogo from '../../assets/images/citibank.png';
import SevenUpLogo from '../../assets/images/7up.png';
import { breakpoints, colors } from '../../theme';
import Underline from '../Underline';

const SponsorsList: React.FC = () => (
  <Container className="container">
    <Title>
      OUR NON-PROFIT THEATRES AND PROGRAMS ARE SPONSORED IN PART BY THE
      FOLLOWING&nbsp;
      <a href="#andyou" title="Go to And You ">
        <Underline>AND YOU</Underline>
      </a>:
    </Title>
    <StyledLogo src={CitiLogo} title="Citi Logo"/>
    <StyledLogo src={MsgLogo} title="MSG Logo"/>
    <StyledLogo src={PolarLogo} title="Polar Logo"/>
    <StyledLogo src={VerndaleLogo} title="Verndale Logo"/>
    <StyledLogo src={SevenUpLogo} title="7up Logo"/>
  </Container>
);

const Container = styled.div`
  padding: 1.25rem 0.625rem 5.625rem 0.625rem;
  @media screen and (min-width: ${breakpoints.tablet}) {
    padding: 2.5rem 0.625rem 1.25rem ;
  }
  @media screen and (min-width: ${breakpoints.desktop}) {
    padding: 2.5rem 0 1.25rem 0;
  }
`;

const Title = styled.h3`
  margin-top: 0;
  margin-bottom: 2.5rem;
  font-size: 0.875rem;
  text-align: center;
  font-weight: 400;
  a {
    color: ${colors.black.main};
  }
  @media screen and (min-width: ${breakpoints.tablet}) {
    text-align: left;
    margin-bottom: 1.5rem;
    font-weight: 500;
  }
`;

const StyledLogo = styled.div<{ src: string }>`
  background-size: contain;
  background-repeat: no-repeat;
  background-position: center;
  background-image: url(${(props) => props.src});
  display: inline-block;
  width: calc(50% - 1rem);
  height: 63px;
  margin: 0 0.5rem 1.5rem;
  &:last-of-type {
    width: 100%;
    margin: 0;
  }
  @media screen and (min-width: ${breakpoints.tablet}) {
    margin: 0;
    &:nth-of-type(1){
      width: 18%;
    }
    &:nth-of-type(2){
      width: 26%;
      margin: 0 4.5%;
    }
    &:nth-of-type(3){
      width: 7%;
    }
    &:nth-of-type(4){
      width: 24%;
      margin: 0 4.5%;
    }
    &:last-of-type {
      width: 7%;
      background-position: center right;
    }
  }
`;

export default React.memo(SponsorsList);
