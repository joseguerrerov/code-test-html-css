export const colors = {
  blue: {
    main: '#2caae2',
    light: '#56bbe8',
  },
  yellow: {
    main: '#fed33b',
    light: '#fedc62',
  },
  white: {
    main: '#FFF',
  },
  black: {
    main: '#000',
  },
};

export const breakpoints = {
  tablet: '768px',
  desktop: '1200px',
};
