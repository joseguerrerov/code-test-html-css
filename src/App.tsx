import React from 'react';
import styled from 'styled-components';
import CalloutList from './components/CalloutList';
import SponsorsList from './components/SponsorsList';
import { breakpoints } from './theme';

const App: React.FC = () => (
  <Container>
    <CalloutList />
    <SponsorsList />
  </Container>
);

const Container = styled.div`
  padding-top: 1.875rem;
  @media screen and (min-width: ${breakpoints.tablet}) {
    padding: 3.5rem 0;
  }
  @media screen and (min-width: ${breakpoints.desktop}) {
    padding-top: 5.375rem;
    padding-bottom: 6.25rem;
  }
`;

export default App;
