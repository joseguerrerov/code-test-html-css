# Verndale HTML-CSS React Code Test
[![CircleCI](https://circleci.com/gh/joseguerrerov/code-test-html-css/tree/master.svg?style=svg&circle-token=a75d04cdb31e8082ee13587dd63c7b8e34ecec1e)](https://circleci.com/gh/joseguerrerov/code-test-html-css/tree/master)

### Description
React & TypeScript SPA built with [styled-components](https://www.styled-components.com/).

### Demo
https://html.test.joseguerrero.dev/

### Build steps

* `npm install` or `yarn`
* `npm run start` o `yarn start`
* open http://localhost:3000/

### Lint

* `npm run tslint` o `yarn tslint`
